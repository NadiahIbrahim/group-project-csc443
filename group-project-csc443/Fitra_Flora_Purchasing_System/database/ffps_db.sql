-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2021 at 04:44 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ffps_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(30) NOT NULL,
  `client_ip` varchar(20) NOT NULL,
  `user_id` int(30) NOT NULL,
  `product_id` int(30) NOT NULL,
  `qty` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category_list`
--

CREATE TABLE `category_list` (
  `id` int(30) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_list`
--

INSERT INTO `category_list` (`id`, `name`) VALUES
(3, 'Flowers'),
(5, 'Cactus');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(30) NOT NULL,
  `name` text NOT NULL,
  `address` text NOT NULL,
  `mobile` text NOT NULL,
  `email` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `address`, `mobile`, `email`, `status`) VALUES
(1, 'Nadiah Ibrahim', 'Lot 221, KG Pinggir Tiga Jaya, Lembah Jaya Utara, Ampang 68000, Selangor', '0179923480', 'nadiahibrahim05@gmail.com', 1),
(2, 'Wan Kamariah Wan Nawang', 'Lot 3, Jalan 4, Kampung Pinggir Tiga, Ampang', '0133190693', 'wankamirah@gmail.com', 1),
(3, 'Kamarul Hisham', 'Taman 3 Jaya, Pandan Indah, Ampang', '0176625905', 'kamarul@gmail.com', 1),
(4, 'Siti Rusydina Najihah Roslan', 'Apartment 7, Tingkat 11, Taman Permai, Kuala Lumpur', '0197296408', 'sitirusydina@gmail.com', 1),
(5, 'Ain Nasuha Bazli', 'No 7, Lorong Cahaya, Taman Cahaya, Selangor', '0126923719', 'ainnasuha@gmail.com', 1),
(6, 'Mohd Ismail Mohd Quyum', 'Lorong 9, Jalan Taman Cempaka, Cempaka, Kuala Lumpur', '0112197330', 'mohdismail@gmail.com', 1),
(7, 'Mohd Safwan Ibrahim', 'Lot 20-2, Kampung Pinggir Tiga Jaya, Lembah Jaya Utara', '0139926788', 'mohdsafwan@gmail.com', 1),
(8, 'Nur Ain Abu Bakar', 'Taman Cahaya, Cahaya Indah, Ampang Selangor', '0129923490', 'nurain@gmail.com', 1),
(9, 'Nur Ain Abu Bakar', 'Taman Cahaya, Cahaya Indah, Ampang Selangor', '0129923490', 'nurain@gmail.com', 1),
(10, 'Nurul Izzah Rijan', 'Kampung Taman Indah, Indah Jaya, Ampang Selangor', '0136602688', 'nurulizzzah@gmail.com', 1),
(11, 'Nadiah Ibrahim', 'Lot 221, KG Pinggir Tiga Jaya, Lembah Jaya Utara, Ampang 68000, Selangor', '0179923480', 'nadiahibrahim05@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` int(30) NOT NULL,
  `order_id` int(30) NOT NULL,
  `product_id` int(30) NOT NULL,
  `qty` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `order_id`, `product_id`, `qty`) VALUES
(1, 1, 3, 1),
(2, 1, 5, 1),
(3, 1, 3, 1),
(4, 1, 6, 3),
(5, 2, 1, 2),
(6, 3, 5, 0),
(8, 4, 7, 1),
(9, 5, 1, 2),
(10, 6, 12, 1),
(11, 6, 8, 1),
(12, 7, 9, 1),
(13, 7, 3, 1),
(14, 8, 8, 1),
(15, 8, 7, 1),
(16, 9, 11, 1),
(17, 10, 12, 1),
(18, 11, 4, 1),
(19, 11, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_list`
--

CREATE TABLE `product_list` (
  `id` int(30) NOT NULL,
  `category_id` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL DEFAULT 0,
  `img_path` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0= unavailable, 2 Available'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_list`
--

INSERT INTO `product_list` (`id`, `category_id`, `name`, `description`, `price`, `img_path`, `status`) VALUES
(1, 3, 'Sun Flowers', 'RM 25\r\nCategory : Flowers', 25, '1627475760_matahari.jpg', 1),
(2, 3, 'Asoka Flowers', 'RM 8\r\nCategory : Flowers', 8, '1627475760_asoka.jpg', 1),
(3, 3, 'Clover Flowers', 'RM 15\r\nCategory : Flowers', 15, '1627475820_semanggi.jpg', 1),
(4, 3, 'Allamanda Flowers', 'RM 10\r\nCategory : Flowers', 10, '1627475880_alamanda.jpg', 1),
(5, 3, 'Anggerik Flowers', 'RM 22\r\nCategory : Flowers', 22, '1627475940_anggerik.jpg', 1),
(6, 3, 'Lonceng Flowers', 'RM 13\r\nCategory : Flowers', 13, '1627475940_lonceng.jpg', 1),
(7, 3, 'Melati Flowers', 'RM 15\r\nCategory : Flowers', 15, '1627476000_melati.jpg', 1),
(8, 3, 'Bougenvilla Flowers', 'RM 8\r\nCategory : Flowers', 8, '1627476000_kertas.jpg', 1),
(9, 3, 'Apel Flowers', 'RM 22\r\nCategory : Flowers', 22, '1627476060_apel.jpeg', 1),
(10, 3, 'Adenium Flowers', 'RM 24\r\nCategory : Flowers', 24, '1627476120_adenium.jpg', 1),
(11, 5, 'Stetsonia Cactus', 'RM 10\r\nCategory : Cactus', 10, '1627476120_stetsonia.jpg', 1),
(12, 5, 'Echinocactus Cactus', 'RM 12\r\nCategory : Cactus', 12, '1627476180_echinocactus.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(30) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `cover_img` text NOT NULL,
  `about_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `name`, `email`, `contact`, `cover_img`, `about_content`) VALUES
(1, 'Fitra Flora Purchasing System', 'fitraflora@yahoo.com', '013-9335205', '1627476300_2.jpg', '&lt;!doctype html&gt;\r\n&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;body background = &gt;\r\n\r\n&lt;meta charset=&quot;utf-8&quot;&gt;\r\n&lt;title&gt;About&lt;/title&gt;\r\n&lt;link rel=&quot;stylesheet&quot; href=&quot;myselfcss.css&quot;&gt;\r\n	\r\n\r\n&lt;link href=&quot;sitestyle.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;&gt;\r\n&lt;/head&gt;\r\n\r\n&lt;body&gt;\r\n	&lt;h1&gt;    &lt;center&gt; FITRA FLORA ENTERPRISE &lt;/h1&gt;\r\n\r\n&lt;center&gt;&lt;td&gt; &lt;/td&gt; &lt;br&gt;Plant Nursery at Kuala Terengganu &lt;td&gt; &lt;br&gt;\r\n	&lt;/n&gt;\r\n	&lt;td&gt;Address:&lt;/td&gt; &lt;td&gt;1593, Jalan Panji Alam, Kampung Gemia, 21100 Kuala Terengganu, Terengganu&lt;/td&gt;\r\n	&lt;/tr&gt;&lt;br&gt;\r\n	&lt;tr&gt;\r\n	&lt;td&gt;Hours:&lt;/td&gt; &lt;td&gt; 8:30AM - 6AM&lt;/td&gt;&lt;br&gt;\r\n	&lt;/tr&gt;\r\n	&lt;tr&gt;\r\n	&lt;td&gt;Phone:&lt;/td&gt; &lt;td&gt;013-933 5205&lt;/td&gt;&lt;br&gt;\r\n	&lt;/tr&gt;\r\n	&lt;tr&gt;\r\n	&lt;td&gt;Platform:&lt;/td&gt; &lt;td&gt;Facebook and Instagram&lt;/td&gt;&lt;br&gt;\r\n	&lt;tr&gt; &lt;/center&gt;\r\n&lt;br&gt;\r\n\r\n&lt;td&gt;\r\n&lt;tr&gt; \r\n&lt;/tr&gt;\r\n	&lt;center&gt;&lt;table width=&quot;50%&quot; border=&quot;1&quot;&gt;\r\n  &lt;tr&gt;\r\n    &lt;td&gt;Sun Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Sunflower is a seasonal plant of the genus Asteraceae that is very popular as an ornamental plant.\r\n	Sunflower also has many benefits, one of the benefits of sunflower is that it can produce\r\n	cauliflower and oil that can be sold by its cultivator.;&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n  &lt;tr&gt;\r\n     &lt;td&gt;Asoka Flowers&lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Asoka flower is a plant that is often used as an ornamental and greening\r\n     plant in gardens. This asoka flower can be used as a cure for bruises by \r\n     drinking boiled water from this asoka flower added with dried roses.;&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n  &lt;tr&gt;\r\n     &lt;td&gt;Clover Flowers &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Clover flowers are a group of aquatic plants (Salviniales) from the genus Marsilea L. This flowering plant has the\r\n     characteristics and shape of an ental that resembles an umbrella, composed of four facing leaves. The leaf shape of this\r\n      flower, called &quot;clover&quot; is used for some other types of dicotyledonous plants that have similar leaves like this clover\r\n       flower, such as clover.;&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n  &lt;tr&gt;\r\n     &lt;td&gt;Allamanda Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Allamanda flower or can be called Allamanda cathartica is an ornamental plant also called gold trumpet flower or buttercup flower.\r\n     This flowering plant is among the tall woody shrubs that can reach up to 2 meters. This Allamanda flower is originally\r\n      from Central and South America, but it is also found in Brazil where this flower is commonly used there as decoration because of its \r\n      very beautiful and fragrant shape.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n  &lt;tr&gt;\r\n     &lt;td&gt;Anggerik Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Anggerik Flower or Orchid are the largest family of flowering trees (Angiosperms). Orchid is the national flower of Singapore. The English name is widely used with the spelling Orchid. Orchids are a flowering plant that is often planted as an ornamental tree. Flowering plants began to appear in the Cretaceous. The first flowering tree is the angiosperm.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n   &lt;tr&gt;\r\n     &lt;td&gt;Lonceng Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Lonceng  flower (Platycodon grandiflorus) is a flower that is an annual plant belonging to the family Campanulaceae and the only\r\n     plant of the genus Platycodon. This flower comes from East Asia Japan, China, Korea, to East Siberia. This Japanese  will produce \r\n     flowers that are blue and large, this bell flower also has a variety of white and pink colors in the cultivation.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n    &lt;tr&gt;\r\n     &lt;td&gt;Melati Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Melati flowers are a type of ornamental plant in the form of upright shrubs that can live in years. \r\n    This flower is a genus of shrubs and plants that grow in the olive family (Oleaceae). This  flower is\r\n     commonly cultivated as an ornamental plant and for the special scent of flowers such as jasmine tea and perfume.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n   &lt;tr&gt;\r\n     &lt;td&gt;Bougenville  Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Bougenville flower is one of the most famous ornamental plants, it has a colorful stalk section. Its beauty\r\n     comes from the seludang (protective leaf) of the flower which is very bright in color and attracts attention\r\n      because it grows with its lush and shady.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n   &lt;tr&gt;\r\n     &lt;td&gt;Apel  Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Apple flowers usually bloom in spring, coinciding with the germination of the leaves. Then the apple \r\n    flowers will be white and blend with the pink color that gradually fades. In this apple flower, there are \r\n    five flower petals that can reach a diameter of 2.5 to 3.5 cm. Apples ripen in the fall, and are generally \r\n    5 to 9 cm in diameter.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n   &lt;tr&gt;\r\n     &lt;td&gt;Adenium  Flower &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;40%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Adenium is a flower that is one of the beautiful and very charming flower ornamental plants, at a glance this \r\n    flower looks like a living painting. This flower has many variants, flowers in each variant also have a distinctive \r\n    color and pattern as well as shape.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n  &lt;td&gt;Stetsonia Cactus  &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;170%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Monovid Stetsonium club-shaped - giant columnar cactus (up to 8 m). The stems are greenish-green with 9 blunt \r\n    ribs; sharp nails exploded from the white stone that felt white.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n  \r\n  &lt;td&gt;Echinocactus Cactus  &lt;br&gt;&lt;a href=&quot;&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;&quot; height=&quot;30%&gt; width=&quot;130%&quot;&lt;/a&gt;&lt;/td&gt;\r\n    &lt;td&gt;Cactus, has the shape of a ball - on young plants, the diameter is equal to the height, with age the plant takes on an \r\n    elongated shape. The average height of the plant is 1.5 m, maximum is 3 m. It has many ribs covered with thorns. Tubular \r\n    flowers appear at the apex and often gather in a bouquet. Echinocactus esimen of about 500 years was recorded, with a mass of 1 ton.&lt;/td&gt; \r\n  &lt;/tr&gt;\r\n \r\n  \r\n  &lt;/table&gt; &lt;/center&gt;\r\n&lt;/body&gt;\r\n&lt;/html&gt;\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(30) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 2 COMMENT '1=admin , 2 = staff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `type`) VALUES
(1, 'Administrator', 'admin', 'admin123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address`) VALUES
(1, 'Nadiah', 'Ibrahim', 'nadiahibrahim05@gmail.com', '681c274f52361e0fa14a8e698ec3e32f', '0179923480', 'Lot 221, KG Pinggir Tiga Jaya, Lembah Jaya Utara, Ampang 68000, Selangor'),
(2, 'Wan Kamariah', 'Wan Nawang', 'wankamirah@gmail.com', '2310eb828cf58635d005e1e9ef7539e5', '0133190693', 'Lot 3, Jalan 4, Kampung Pinggir Tiga, Ampang'),
(3, 'Kamarul', 'Hisham', 'kamarul@gmail.com', 'f5b3b860399ae6f635c953f99504f9f8', '0176625905', 'Taman 3 Jaya, Pandan Indah, Ampang'),
(4, 'Siti Rusydina Najihah', 'Roslan', 'sitirusydina@gmail.com', '4a9d85d175bc0c6e9265183915f2e693', '0197296408', 'Apartment 7, Tingkat 11, Taman Permai, Kuala Lumpur'),
(5, 'Ain Nasuha', 'Bazli', 'ainnasuha@gmail.com', '3ee3c01f0a42362da2536b3e9bcd7121', '0126923719', 'No 7, Lorong Cahaya, Taman Cahaya, Selangor'),
(6, 'Mohd Ismail', 'Mohd Quyum', 'mohdismail@gmail.com', '69f8624b379af0cd3a0db3dae8d6f568', '0112197330', 'Lorong 9, Jalan Taman Cempaka, Cempaka, Kuala Lumpur'),
(7, 'Mohd Safwan', 'Ibrahim', 'mohdsafwan@gmail.com', 'f7ab839f94a07af69ddd64843ea67c7e', '0139926788', 'Lot 20-2, Kampung Pinggir Tiga Jaya, Lembah Jaya Utara'),
(8, 'Nur Ain', 'Abu Bakar', 'nurain@gmail.com', '99c8fc0fa579d5a70be5e4ae0cc50dce', '0129923490', 'Taman Cahaya, Cahaya Indah, Ampang Selangor'),
(9, 'Nurul Izzah', 'Rijan', 'nurulizzzah@gmail.com', '6f208f17eb73b4509b75e526dd5dec42', '0136602688', 'Kampung Taman Indah, Indah Jaya, Ampang Selangor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_list`
--
ALTER TABLE `category_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_list`
--
ALTER TABLE `product_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `category_list`
--
ALTER TABLE `category_list`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `product_list`
--
ALTER TABLE `product_list`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
